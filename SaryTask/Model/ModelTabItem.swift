//
//  ModelTabItem.swift
//  SaryTask
//
//  Created by iSlam on 1/27/20.
//  Copyright © 2020 iSlamAbdel-Aziz. All rights reserved.
//

import Foundation

struct ModelTabItem {
    var title: String?
    var isSelected: Bool = false
}
