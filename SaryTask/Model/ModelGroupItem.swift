/* 
Copyright (c) 2020 Swift Models Generated from JSON powered by http://www.json4swift.com

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

For support, please feel free to contact me at https://www.linkedin.com/in/syedabsar

*/

import Foundation
import ObjectMapper

struct ModelGroupItem : Mappable {
	var package_id : Int?
	var title : String?
	var sub_title : String?
	var sku : String?
	var price : String?
	var quantity : String?
	var image : String?
	var flavor_text : String?
	var is_favored : Bool?
	var is_available : Bool?
	var consumable_display : String?
	var saving_display : String?
	var saving_amount : String?
	var saving_percentage : String?
	var old_price : String?
	var priority : Int?

	init?(map: Map) {

	}

	mutating func mapping(map: Map) {

		package_id <- map["package_id"]
		title <- map["title"]
		sub_title <- map["sub_title"]
		sku <- map["sku"]
		price <- map["price"]
		quantity <- map["quantity"]
		image <- map["image"]
		flavor_text <- map["flavor_text"]
		is_favored <- map["is_favored"]
		is_available <- map["is_available"]
		consumable_display <- map["consumable_display"]
		saving_display <- map["saving_display"]
		saving_amount <- map["saving_amount"]
		saving_percentage <- map["saving_percentage"]
		old_price <- map["old_price"]
		priority <- map["priority"]
	}

}
