//
//  ItemsCVcell.swift
//  SaryTask
//
//  Created by iSlam on 1/27/20.
//  Copyright © 2020 iSlamAbdel-Aziz. All rights reserved.
//

import UIKit

class ItemsCVcell: UICollectionViewCell {

    @IBOutlet weak var itemImg: UIImageView!
    @IBOutlet weak var offerLbl: UILabel!
    @IBOutlet weak var infoLbl: UILabel!
    @IBOutlet weak var savingLbl: UILabel!
    @IBOutlet weak var titleLbl: UILabel!
    @IBOutlet weak var subTitleLbl: UILabel!

    var dataObj: ModelGroupItem?{
        didSet{
            itemImg.setImage(imageUrl: dataObj?.image ?? "")
            if dataObj?.is_available ?? false{
                offerLbl.text =  dataObj?.saving_display
                offerLbl.setGradientColor(isVertical: false, colors: [UIColor.grediantTop, UIColor.gradientBottom])
            }else{
                offerLbl.text =  "غير متاح حاليا"
                offerLbl.backgroundColor = UIColor.offerColor
            }
            
//            let attribut = NSMutableAttributedString(string: "\("Call Center".localized)\n\n\(num)")
//            attribut.addAttributes([NSAttributedString.Key.foregroundColor: UIColor.darkGray, NSAttributedString.Key.font: UIFont.systemFont(ofSize: 13)], range: NSRange(location: "\("Call Center".localized)\n\n".count, length: "\(num)".count))
//            cell.txtLbl.attributedText = attribut


            if let price = dataObj?.price, let title = dataObj?.title{
                let currency = "ريال"
                
                if let old = dataObj?.old_price{
                    var text = NSMutableAttributedString(string: "\(price) \(currency) \(old)", attributes: [:])
                    text.addAttributes([NSAttributedString.Key.foregroundColor: UIColor.black], range: NSRange(location: 0, length: "\(price) \(currency)".count))
                    text.addAttributes([NSAttributedString.Key.font: UIFont(name: "Cairo-SemiBold", size: 22)!], range: NSRange(location: 0, length: "\(price) ".count))
                    text.addAttributes([NSAttributedString.Key.font: UIFont(name: "Cairo-SemiBold", size: 9)!], range: NSRange(location: "\(price) ".count, length: "\(currency)".count))
                    text.addAttributes([NSAttributedString.Key.foregroundColor: UIColor.lightGray, NSAttributedString.Key.font: UIFont(name: "Cairo-SemiBold", size: 9)!,
                                        NSAttributedString.Key.strikethroughStyle: 1], range: NSRange(location: "\(price) \(currency) ".count, length: "\(old)".count))
                    infoLbl.attributedText = text

                }else{
                    var text = NSMutableAttributedString(string: "\(price) \(currency)", attributes: [:])
                    text.addAttributes([NSAttributedString.Key.foregroundColor: UIColor.black], range: NSRange(location: 0, length: "\(price) \(currency)".count))
                    text.addAttributes([NSAttributedString.Key.font: UIFont(name: "Cairo-SemiBold", size: 22)!], range: NSRange(location: 0, length: "\(price) ".count))
                    text.addAttributes([NSAttributedString.Key.font: UIFont(name: "Cairo-SemiBold", size: 9)!], range: NSRange(location: "\(price) ".count, length: "\(currency)".count))
                    infoLbl.attributedText = text
                }

            }

            
            savingLbl.text = dataObj?.saving_display
            titleLbl.text = dataObj?.title
            subTitleLbl.text = dataObj?.sub_title
            
            
        }
    }

    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

}
