//
//  FilterCVcell.swift
//  SaryTask
//
//  Created by iSlam on 1/27/20.
//  Copyright © 2020 iSlamAbdel-Aziz. All rights reserved.
//

import UIKit

class FilterCVcell: UICollectionViewCell {
    
    @IBOutlet weak var titleLbl: UILabel!

    
    var dataObj: ModelFilters?{
        didSet{
            titleLbl.text = dataObj?.name
            if dataObj?.isSelected ?? false{
                setGradientColor(isVertical: true, colors: [UIColor.grediantTop, UIColor.gradientBottom])
                titleLbl.textColor = UIColor.white
            }else{
                backgroundColor = UIColor.white
                titleLbl.textColor = UIColor.black
            }
        }
    }
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        addCornerRadius(14)
    }

}
