//
//  GroupsVC.swift
//  SaryTask
//
//  Created by iSlam on 1/27/20.
//  Copyright © 2020 iSlamAbdel-Aziz. All rights reserved.
//

import UIKit

class GroupsVC: UIViewController {
    
    @IBOutlet weak var tabsCV: UICollectionView!
    @IBOutlet weak var filtersCV: UICollectionView!
    @IBOutlet weak var itemsCV: UICollectionView!

    var viewModel = GroupsViewModel()

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        tabsCV.register(TabCVcell.nib, forCellWithReuseIdentifier: TabCVcell.identifier)
        filtersCV.register(FilterCVcell.nib, forCellWithReuseIdentifier: FilterCVcell.identifier)
        itemsCV.register(ItemsCVcell.nib, forCellWithReuseIdentifier: ItemsCVcell.identifier)
//        filterBtn.addCornerRadius()
        navigationItem.title = "Groups".localized
        
        bindViewModel()
        viewModel.fetchGroupDetails()
//        viewModel.fetchGroupItems()
        
    }
    

    
    
    func bindViewModel(){
        viewModel.arrayOfGroups.bind { (_) in
            self.tabsCV.reloadData()
        }
        
        viewModel.arrayOfFilters.bind { (_) in
            self.filtersCV.reloadData()
        }
        
        viewModel.arrayOfItems.bind { (_) in
            self.itemsCV.reloadData()
        }
        
        viewModel.errorMsg.bind { (msg) in
            print(msg)
        }
    }
}
