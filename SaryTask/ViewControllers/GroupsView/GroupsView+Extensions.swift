//
//  GroupsView+Extensions.swift
//  SaryTask
//
//  Created by iSlam on 1/27/20.
//  Copyright © 2020 iSlamAbdel-Aziz. All rights reserved.
//

import UIKit

extension GroupsVC: UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout{
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if collectionView === tabsCV{
            return viewModel.arrayOfGroups.value.count
        }
        if collectionView === filtersCV{
            return viewModel.arrayOfFilters.value.count
        }
        if collectionView === itemsCV{
            return viewModel.arrayOfItems.value.count
        }
        return 0
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if collectionView === tabsCV{
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: TabCVcell.identifier, for: indexPath) as! TabCVcell
            cell.dataObj = viewModel.arrayOfGroups.value[indexPath.row]
            return cell
        }
        if collectionView === filtersCV{
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: FilterCVcell.identifier, for: indexPath) as! FilterCVcell
            cell.dataObj = viewModel.arrayOfFilters.value[indexPath.row]
            return cell
        }
        
        if collectionView === itemsCV{
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: ItemsCVcell.identifier, for: indexPath) as! ItemsCVcell
            cell.dataObj = viewModel.arrayOfItems.value[indexPath.row]
            return cell
        }
        
        return UICollectionViewCell()
        
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        if collectionView === tabsCV, let _ = collectionView.cellForItem(at: indexPath) as? TabCVcell{
            var arr = viewModel.arrayOfGroups.value
            for i in 0 ... arr.count - 1{
                if i == indexPath.row{
                    arr[i].isSelected = true
                    viewModel.arrayOfFilters.value = viewModel.arrayOfGroups.value[indexPath.row].filters ?? []
                }else{
                    arr[i].isSelected = false
                }
            }
            self.viewModel.arrayOfGroups.value = arr
            DispatchQueue.main.async {
                collectionView.scrollToItem(at: indexPath, at: .centeredHorizontally, animated: true)
            }
        }
        
        if collectionView === filtersCV, let _ = collectionView.cellForItem(at: indexPath) as? FilterCVcell{
            var arr = viewModel.arrayOfFilters.value
            for i in 0 ... arr.count - 1{
                if i == indexPath.row{
                    arr[i].isSelected = true
                }else{
                    arr[i].isSelected = false
                }
            }
            self.viewModel.arrayOfFilters.value = arr
            viewModel.fetchGroupItems()
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        if collectionView === tabsCV{
            return CGSize(width: collectionView.bounds.width / 4, height: 38)
        }
        if collectionView === filtersCV{
            return CGSize(width: collectionView.bounds.width / 4, height: 28)
        }
        if collectionView === itemsCV{
            let width = collectionView.bounds.width / 2.2
            return CGSize(width: width, height: width * 1.4)
        }
        return CGSize(width: 0, height: 0)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, referenceSizeForFooterInSection section: Int) -> CGSize {
        if collectionView === itemsCV{
            return CGSize(width: collectionView.bounds.width, height: 80)
        }
        return CGSize(width: 0, height: 0)
    }
    
    
}


