//
//  GroupsViewModel.swift
//  SaryTask
//
//  Created by iSlam on 1/27/20.
//  Copyright © 2020 iSlamAbdel-Aziz. All rights reserved.
//

import Foundation

class GroupsViewModel {
    
    var arrayOfGroups: Bindable<[ModelGroup]> = Bindable([ModelGroup]())
    var arrayOfFilters: Bindable<[ModelFilters]> = Bindable([ModelFilters]())
    var arrayOfItems: Bindable<[ModelGroupItem]> = Bindable([ModelGroupItem]())
    var errorMsg: Bindable<String?> = Bindable(nil)
    
    
    func fetchGroupDetails(){
        API().getGroupDetails { (isDone, msg, arr) in
            if isDone{
                if let groups = arr{
                    self.arrayOfGroups.value = groups
                }
            }else{
                self.errorMsg.value = msg
            }
        }
    }
    
    func fetchGroupItems(){
        API().getGroupItems { (isDone, msg, arr) in
            if isDone{
                if let items = arr{
                    self.arrayOfItems.value = items
                }
            }else{
                self.errorMsg.value = msg
            }
        }
    }
}
