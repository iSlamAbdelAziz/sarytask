//
//  TabCVcell.swift
//  SaryTask
//
//  Created by iSlam on 1/27/20.
//  Copyright © 2020 iSlamAbdel-Aziz. All rights reserved.
//

import UIKit

class TabCVcell: UICollectionViewCell {
    
    @IBOutlet weak var titleLbl: UILabel!
    @IBOutlet weak var selectionView: UIView!

    var dataObj: ModelGroup?{
        didSet{
            if let val = dataObj?.isSelected{
                print(val)
                selectionView.isHidden = !val
                if val{
                    titleLbl.textColor = UIColor.grediantTop
                }else{
                    titleLbl.textColor = UIColor.black
                }
            }else{
                selectionView.isHidden = false
                titleLbl.textColor = UIColor.black
            }
            selectionView.setGradientColor(isVertical: true, colors: [UIColor.grediantTop, UIColor.gradientBottom])

            titleLbl.text = dataObj?.name
        }
    }

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

}
