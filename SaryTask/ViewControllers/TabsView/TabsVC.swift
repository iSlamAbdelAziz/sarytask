//
//  TabsVC.swift
//  SaryTask
//
//  Created by iSlam on 1/27/20.
//  Copyright © 2020 iSlamAbdel-Aziz. All rights reserved.
//

import UIKit
import PolioPager

class TabsVC: PolioPagerViewController {

    //MARK:- DidLoad, DidAppear, ... etc
    override func viewDidLoad() {
        self.needSearchTab = false
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        navigationItem.title = "Groups".localized
    }
    
    
    //MARK:- Handle Tabs
    override func tabItems()-> [TabItem] {
        return [TabItem(title: "Redbull"), TabItem(title: "Monster"), TabItem(title: "Monster")]
    }

    override func viewControllers()-> [UIViewController]
    {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)

        let viewController1 = storyboard.instantiateViewController(withIdentifier: "v2")
        let viewController2 = storyboard.instantiateViewController(withIdentifier: "GroupsVC")
        let viewController3 = storyboard.instantiateViewController(withIdentifier: "v3")

        return [viewController1, viewController2, viewController3]
    }



}
