//
//  DataClass.swift
//  Occasion
//
//  Created by iSlam on 5/25/19.
//  Copyright © 2019 iSlam. All rights reserved.
//

import Alamofire



typealias JSON = [String : Any]
typealias JSONArray = [JSON]

struct NetworkingManager {
    
    static let shared: SessionManager = {
        let configuration = URLSessionConfiguration.default
        configuration.timeoutIntervalForRequest = 30
        configuration.timeoutIntervalForResource = 30
        let sessionManager = Alamofire.SessionManager(configuration: configuration)        
        return sessionManager
    }()
    
}



class DataClass{
    
    
    
    //MARK;- Send Request
    /**
     This Function is used as an abstract to handle API requests using Alamofire
     - parameter method: The HTTP Methoud To Use
     - parameter webService: is the name of service to call like login, register, getData in Type of String
     - parameter param: this is a Dictionary contains all data that you need to send this request
     - parameter showLoading: Do you want to Show Loading Hud ?
     - parameter closure: This is the final result that you can get data from request using it
     */
    class func sendRequest(method: HTTPMethod, webService: String, param: [String: Any], showLoading: Bool = true, headers: JSON, closure: @escaping ([String: Any]?) -> Void){
        
        #if DEBUG
        print("URL:\(webService)")
        //        print("Params: \(param)")
        #endif
        if showLoading{
            LoadingOverlay.showOverlay()
        }
        
        let URL1 = Constants.baseURL + webService
        
        
//        let headers = NetworkingController.getHeaders()
        
        
        NetworkingManager.shared.request(URL1, method: method, parameters: param, encoding: JSONEncoding.default, headers: headers as! HTTPHeaders).responseJSON { response in

            if showLoading{
                LoadingOverlay.hideOverlay()
            }

            #if DEBUG
            print("Response: \(response.result.value)")
            #endif
            switch response.result{
            case .success(_):
                if let res = response.result.value as? [String: Any]{
                    closure(res)
                }else{
                    closure(nil)
                }

            case .failure(_):
                print(response.result.value)

                if let res = response.result.value as? [String: Any]{
                    closure(res)
                }else{
                    closure(nil)
                }
            }

        }
        
    }
    
    
    
    
    class func sendRequestUsingAlamofire(method: HTTPMethod, webService: String, param: [String: Any], showLoading: Bool = true, headers: JSON, closure: @escaping ([String: Any]?) -> Void){
        
        #if DEBUG
        print("URL:\(webService)")
        //        print("Params: \(param)")
        #endif
        if showLoading{
            LoadingOverlay.showOverlay()
        }
        
        let URL1 = Constants.baseURL + webService
        
        
//        let headers: [String: String] = [:]
        
        Alamofire.request(URL1, method: method, parameters: param, encoding: URLEncoding.default, headers: headers as! HTTPHeaders).responseJSON { response in
            
            if showLoading{
                LoadingOverlay.hideOverlay()
            }
            
            #if DEBUG
            print("Response: \(response.result.value)")
            #endif
            switch response.result{
            case .success(_):
                if let res = response.result.value as? [String: Any]{
                    closure(res)
                }else{
                    closure(nil)
                }
                
            case .failure(_):
                print(response.result.value)
                
                if let res = response.result.value as? [String: Any]{
                    closure(res)
                }else{
                    closure(nil)
                }
            }
            
        }
        
        
    }
    
    
}
