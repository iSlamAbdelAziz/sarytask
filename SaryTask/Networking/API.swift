//
//  API.swift
//  SaryTask
//
//  Created by iSlam on 1/27/20.
//  Copyright © 2020 iSlamAbdel-Aziz. All rights reserved.
//

import Alamofire

class API{
    
    
    ///https://staging.sary.co/api/baskets/28736/group/161
    func getGroupDetails(completion: @escaping(Bool, String, [ModelGroup]?)-> Void){
        
        DataClass.sendRequestUsingAlamofire(method: .get, webService: Constants.groupDetails, param: [:], showLoading: true, headers: [ "Accept-Language":"ar", "App-Version": "2.7.1.1.0" ]) { (result) in
            if let data = result{
                if let state = data["status"] as? Bool, state{
                    if let arrDic = data["result"] as? [JSON]{
                        var arr = [ModelGroup]()
                        for item in arrDic{
                            if let obj = ModelGroup(JSON: item){
                                arr.append(obj)
                            }
                        }
                        completion(true, "Success", arr)
                    }
                }else{
                    completion(false, data["message"] as? String ?? "Error", nil)
                }
            }else{
                completion(false, "Error".localized, nil)
            }
        }
        
        
        
        
    }
    
    
    func getGroupItems(completion: @escaping(Bool, String, [ModelGroupItem]?)-> Void){
        
        DataClass.sendRequestUsingAlamofire(method: .get, webService: Constants.groupItem, param: [:], showLoading: true, headers: [ "Accept-Language":"ar", "App-Version": "2.7.1.1.0" ]) { (result) in
            if let data = result{
                if let state = data["status"] as? Bool, state{
                    if let arrDic = data["result"] as? [JSON]{
                        var arr = [ModelGroupItem]()
                        for item in arrDic{
                            if let obj = ModelGroupItem(JSON: item){
                                arr.append(obj)
                            }
                        }
                        completion(true, "Success", arr)
                    }
                }else{
                    completion(false, data["message"] as? String ?? "Error", nil)
                }
            }else{
                completion(false, "Error".localized, nil)
            }
        }
        
        
        
        
    }
    
    
}
