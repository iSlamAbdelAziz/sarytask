//
//  StringExtensions.swift
//  Occasion
//
//  Created by iSlam on 5/25/19.
//  Copyright © 2019 iSlam. All rights reserved.
//

import Foundation

extension String{
    var localized: String {
        return NSLocalizedString(self, comment: self)
    }
    
    func trimmedLength() -> Int {
        return self.trimmingCharacters(in: .whitespacesAndNewlines).count
    }
    
    var trimmed: String {
        return trimmingCharacters(in: CharacterSet.whitespacesAndNewlines)
    }
    
    subscript (i: Int) -> Character {
        return self[index(startIndex, offsetBy: i)]
    }


}
