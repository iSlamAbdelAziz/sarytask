//
//  UIColorExtenstions.swift
//  Occasion
//
//  Created by iSlam on 5/25/19.
//  Copyright © 2019 iSlam. All rights reserved.
//

import UIKit


extension UIColor{
    
    @nonobjc class var secondryColor: UIColor {
        return UIColor.init(named: "secondryColor")!
    }
    
    @nonobjc class var gradientBottom: UIColor {
        return UIColor.init(named: "gradientBottom")!
    }
    
    @nonobjc class var grediantTop: UIColor {
        return UIColor.init(named: "grediantTop")!
    }
    
    @nonobjc class var offerColor: UIColor {
        return UIColor.init(named: "offerColor")!
    }
    
    
}



