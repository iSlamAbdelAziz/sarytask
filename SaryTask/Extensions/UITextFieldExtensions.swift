//
//  UITextFieldExtensions.swift
//  Occasion
//
//  Created by iSlam on 5/25/19.
//  Copyright © 2019 iSlam. All rights reserved.
//

import UIKit

extension UITextField{
    func addIconView(img: UIImage, isRight: Bool = false, customWidth: CGFloat = 14, customHeight: CGFloat = 20, customYpos: CGFloat = 0, customXpos: CGFloat = 13){
        let view = UIView(frame: CGRect(x: 0, y: 0, width: 40, height: 20))
        let imgV = UIImageView(frame: CGRect(x: customXpos, y: customYpos, width: customWidth, height: customHeight))
        view.addSubview(imgV)
        imgV.image = img
        imgV.contentMode = .scaleAspectFit
        if isRight{
            self.rightView = view
            self.rightViewMode = .always

        }else{
            self.leftView = view
            self.leftViewMode = .always

        }
        //        self.leftView!.backgroundColor = UIColor.red
        
    }
    
    func addEmptyView(isLeft: Bool, width: Int){
        let view = UIView(frame: CGRect(x: 0, y: 0, width: width, height: 20))
        if isLeft{
            self.leftView = view
            self.leftViewMode = .always
        }else{
            self.rightView = view
            self.rightViewMode = .always
        }
    }
}
