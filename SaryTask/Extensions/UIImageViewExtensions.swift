//
//  UIImageViewExtensions.swift
//  Occasion
//
//  Created by iSlam on 6/22/19.
//  Copyright © 2019 iSlam. All rights reserved.
//

import UIKit
import SDWebImage

extension UIImageView{
    func setImage(imageUrl: String){
        self.sd_setImage(with: URL(string: imageUrl), placeholderImage: UIImage.init(named: NoImageName), options: .continueInBackground) { (imagee, _,_ , _) in
            if let img = imagee{
                self.image = img
            }else{
                self.image = UIImage.init(named: NoImageName)
            }
        }
    }
}
