//
//  File.swift
//  Occasion
//
//  Created by iSlam on 5/25/19.
//  Copyright © 2019 iSlam. All rights reserved.
//

import UIKit
import SVProgressHUD


class LoadingOverlay {
    static func showOverlay() {
        UIApplication.shared.beginIgnoringInteractionEvents()
        SVProgressHUD.show()
    }
    
    static func hideOverlay() {
        UIApplication.shared.endIgnoringInteractionEvents()
        SVProgressHUD.dismiss()
    }
}
